### Climb King

Welcome to the Climb King readme! This document provides an overview of the problem statement, the expected solution, and examples to help understand the implementation and working of the Climb King algorithm.

### Class: `FloorGraph`

A class named `FloorGraph` which represents the map of each floor of the tower. This class has two main methods:

1. **`__init__(self, paths, keys)`**: Initializes the floor graph with paths and keys.
   
   - `paths`: A list of tuples `(u, v, x)` representing paths between locations, where `u` and `v` are location IDs and `x` is the time needed to traverse the path.
   - `keys`: A list of tuples `(k, y)` representing keys at specific locations, where `k` is the location ID and `y` is the time needed to defeat the monster and retrieve the key.

2. **`climb(self, start, exits)`**: Finds the optimal route from the start to one of the exit points.

   - `start`: The starting location ID.
   - `exits`: A list of exit points where you can move on to the next floor.

This function returns one of the shortest routes from the start to one of the exit points that leads to the next floor. The route includes defeating one monster and collecting the key to go up to the next floor. The function returns a tuple of `(total_time, route)`, where `total_time` is the time taken to complete the route and `route` is the shortest route as a list of integers representing the location IDs along the path.

### Examples

#### Example 1:
```python
# Example floor map
paths = [(0, 1, 4), (1, 2, 2), (2, 3, 3), ...]  # Paths between locations
keys = [(5, 10), (6, 1), (7, 5), ...]           # Keys at specific locations

myfloor = FloorGraph(paths, keys)
start = 1
exits = [7, 2, 4]
myfloor.climb(start, exits)
# Output: (9, [1, 7])
```

### Optimal Route Function

 `climb(self, start, exits)` is a function within the `FloorGraph` class. 
 
 The function accepts two arguments:

- `start`: A non-negative integer representing the starting point in the floor.
- `exits`: A non-empty list of non-negative integers representing the exit points in the floor, in order to move on to the next floor.

#### Examples:

```python
# Example floor map
# Paths represented as a list of tuples
paths = [(0, 1, 4), (1, 2, 2), (2, 3, 3), ...]
# Keys represented as a list of tuples
keys = [(5, 10), (6, 1), (7, 5), ...]

# Creating a FloorGraph object based on the given paths and keys
myfloor = FloorGraph(paths, keys)

# Example 1.1
start = 1
exits = [7, 2, 4]
myfloor.climb(start, exits)
# Output: (9, [1, 7])

# Example 1.2
start = 7
exits = [8]
myfloor.climb(start, exits)
# Output: (6, [7, 8])

# Example 1.3
start = 1
exits = [3, 4]
myfloor.climb(start, exits)
# Output: (10, [1, 5, 6, 3])

# Example 1.4
start = 1
exits = [0, 4]
myfloor.climb(start, exits)
# Output: (11, [1, 5, 6, 4])

# Example 1.5
start = 3
exits = [4]
myfloor.climb(start, exits)
# Output: (20, [3, 4, 8, 7, 3, 4])
```

### Complexity

- The `__init__` constructor runs in O(|V| + |E|) time and space, where |V| is the number of unique locations and |E| is the number of paths.
- The `climb` method runs in O(|E| log |V|) time and O(|V| + |E|) space.

### Notes

- This program returns any valid solution if multiple optimal routes exist.

For further assistance or inquiries, please reach out to the developer.

Developer: Anushka Reddy
Contact: anushkar614@gmail.com
