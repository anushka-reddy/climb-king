import heapq

class FloorGraph :
    def __init__(self, paths, keys) :
        """
        Function description: This constructor initializes the FloorGraph object with given paths (or edges) and keys
        (or nodes) which forms the floor map of the tower for the climb function and sets up a list of keys with their
        associated times.

        Approach description: It sets the number of locations (or vertices), V, as one more than the length of paths (to
        accommodate zero-based indexing). Then it creates an adjacency list representation of the graph. Each location
        (or vertex) has a list of neighboring locations (or vertices) and the weight of the edge connecting them which
        is essentially the time taken to travel from one location to another. Then it initializes a keys list of size V
        with all values set to infinity. This list will store the time taken to pick up a key at each vertex. Lastly,
        it updates the keys list with the actual time values from the provided keys input.

        :Input:
        paths: A list of tuples (u, v, w) representing edges or paths between nodes or locations u and v with weight or
        time taken to travel from point u to v as w.
        keys: A list of tuples (k, t) where node or location k has a key that takes t time to pick up in order to defend
        the monster for getting the key.

        :Output, return or post condition: Initializes the FloorGraph object.

        :Time complexity: Building the graph takes O(V) for initializing and O(E) for populating. The sum of degrees
        across all vertices is equal to twice the number of edges (considering undirected graph). Initializing the keys
        list takes O(V). Thus, the time complexity is O(V + E) where V is the total number of locations (or nodes),
        which is given as len(paths) + 1 in the __init__ method of the class and E corresponds to the total number of
        paths (or edges) defined in the input 'paths'.

        :Aux space complexity: 'self.graph': O(V + E) and 'self.keys': O(V). So, the space complexity is O(V + E) where
        V is the total number of locations (or nodes), which is given as len(paths) + 1 in the __init__ method of the
        class and E corresponds to the total number of paths (or edges) defined in the input 'paths'.
        """
        self.V = len(paths) + 1  # Number of nodes
        self.graph = [[] for _ in range(self.V)]
        for u, v, w in paths :
            self.graph[u].append((v, w))

        # Initialize all keys to a very large number (effectively infinity)
        self.keys = [float('inf')] * self.V
        for k, t in keys :
            self.keys[k] = t

    def climb(self, start, exits) :
        """
        Function description: This function determines the shortest time (and corresponding path) it takes to move from
        the start vertex to any of the vertices in exits while also picking up a key, accounting also for the time taken
        to defend the monster to get the key.

        Approach description: It initializes a 'dists' list to store the shortest distance from the start to every other
        vertex. Each entry in dists has two values: the shortest distance without a key and the shortest distance with a
        key. Using a priority queue 'pq' to keep track of vertices to be processed. The queue entries are tuples with
        the format (distance, current_vertex, has_key, path_so_far). Then it begins the Dijkstra-like loop, popping the
        vertex with the shortest distance from the queue. If the popped vertex is in the exits list and has a key, it
        returns the distance and path as the solution. Otherwise, it explores all the neighbors of the current vertex by
        calculating the new distance to this neighbor and checking if it's beneficial to pick up a key at this neighbor
        vertex. If yes, then it updates the distance and path accordingly and pushes it to the queue. If it's not
        beneficial to pick up the key, or if already have a key, then it updates the distance and path without picking
        the key and pushes to the queue. If the loop completes without finding a path, it returns None indicating
        there's no valid path to the exits with a key.

        :Input:
        start: The starting node or location.
        exits: A list of exit nodes or exit locations.

        :Output, return or post condition: A tuple (distance, path) where 'distance' is the minimum time to reach an
        exit with a key and 'path' is the list of nodes or locations visited.

        :Time complexity: The loop runs for all the edges and nodes in the worst case scenario. The while loop in the
        function with 'heapq.heappop(pq)' and 'heapq.heappush(pq)' operations results in a time complexity of
        O(E + V) * log(V). So, the time complexity is O((E+V) log(V)) which is equal to O(|E| log |V|) where V
        represents the number of locations (or nodes) in the floor map and E represents the number of paths (or edges)
        in the floor map.

        :Aux space complexity: The maximum space used is for 'dists': O(|V|) and 'pq': O(|V|). Therefore, the space
        complexity is  O(|V|) where V represents the number of locations (or nodes) in the floor map.
        """
        dists = [[float('inf'), float('inf')] for _ in range(self.V)]
        dists[start][0] = 0

        pq = [(0, start, False, [])]  # (distance, node, has_key, path)

        while pq:
            dist, curr, has_key, path = heapq.heappop(pq)

            if curr in exits and has_key:
                return dist, path + [curr]

            for neighbor, weight in self.graph[curr]:
                new_dist = dist + weight

                if self.keys[neighbor] != float('inf'):
                    with_key_dist = new_dist + self.keys[neighbor]
                    if with_key_dist < dists[neighbor][1]:
                        dists[neighbor][1] = with_key_dist
                        heapq.heappush(pq, (with_key_dist, neighbor, True, path + [curr]))

                # Continue without picking up the key or if already have a key
                if new_dist < dists[neighbor][int(has_key)]:
                    dists[neighbor][int(has_key)] = new_dist
                    heapq.heappush(pq, (new_dist, neighbor, has_key, path + [curr]))

        return None
